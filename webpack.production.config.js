// Dependencies
const webpack = require("webpack");
const path = require("path");
const ProgressBarPlugin = require("progress-bar-webpack-plugin");
const chalk = require("chalk");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const VueLoaderPlugin = require("vue-loader/lib/plugin");
const ZipWebpackPlugin = require("zip-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CompressionWebpackPlugin = require("compression-webpack-plugin");
const VuetifyLoaderPlugin = require("vuetify-loader/lib/plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");


// Directories
const srcFolder = path.resolve(__dirname, "src");
const distFolder = path.resolve(__dirname, "dist");


// Webpack configurations
module.exports = {

    mode: "production",

    devtool: "source-map",

    entry: {
        app: path.resolve(srcFolder, "js/app.js")
    },

    output: {
        path: distFolder,
        filename: "js/[name].bundle.js",
        chunkFilename: "js/[name].chunk.js"
    },

    module: {

        rules: [

            {
                test: /\.s(c|a)ss$/,
                use: [{
                        loader: MiniCssExtractPlugin.loader
                    },
                    {
                        loader: "css-loader",
                        options: {
                            sourceMap: true
                        }
                    },
                    {
                        loader: "sass-loader",
                        options: {
                            implementation: require("sass"),
                            sourceMap: true,
                            sassOptions: {
                                fiber: require("fibers")
                            }
                        }
                    }
                ]
            },

            {
                test: /\.css$/,
                use: [{
                        loader: MiniCssExtractPlugin.loader
                    },
                    {
                        loader: "css-loader",
                        options: {
                            sourceMap: true
                        }
                    },
                    {
                        loader: "postcss-loader",
                        options: {
                            sourceMap: true
                        }
                    }
                ]
            },

            {
                test: /\.(gif|png|jpe?g|svg)$/i,
                use: [{
                        loader: "file-loader",
                        options: {
                            name: "[name].[ext]",
                            outputPath: "images"
                        }
                    },
                    {
                        loader: "image-webpack-loader",
                        options: {
                            mozjpeg: {
                                quality: 75
                            }
                        }
                    }
                ]
            },

            {
                test: /\.vue$/,
                loader: "vue-loader"
            },

            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: "babel-loader"
            }

        ]
    },

    plugins: [

        new ProgressBarPlugin({
            format: "  build [:bar] " + chalk.green.bold(":percent") + " (:elapsed seconds)",
            clear: false
        }),

        new CleanWebpackPlugin(),

        new HtmlWebpackPlugin({
            template: path.resolve(srcFolder, "index.html"),
            favicon: path.resolve(srcFolder, "favicon.ico"),
            minify: {
                html5: true,
                minifyCSS: true,
                minifyJS: true,
                collapseWhitespace: true,
                removeComments: true,
                keepClosingSlash: true
            }
        }),

        new VuetifyLoaderPlugin(),

        new VueLoaderPlugin(),

        new ZipWebpackPlugin({
            path: "zip",
            filename: "app.zip"
        }),

        new webpack.DefinePlugin({
            _TARGET_: JSON.stringify("PRODUCTION")
        }),

        new MiniCssExtractPlugin({
            filename: "css/[name].styles.css",
            chunkFilename: "css/[name].chunk.css"
        }),

        new CompressionWebpackPlugin()

    ]

};