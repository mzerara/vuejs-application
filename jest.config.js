// Dependencies
const path = require("path");


// Directories
const srcFolder = path.resolve(__dirname, "src");


// Jest configurations
module.exports = {

    "verbose": true,

    "moduleNameMapper": {
        "\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$": path.resolve(srcFolder, "js/tests/__mocks__/fileMock.js"),
        "\\.(css|less)$": path.resolve(srcFolder, "js/tests/__mocks__/styleMock.js")
    },

    "moduleFileExtensions": [
        "js",
        "vue"
    ],

    "transform": {
        ".*\\.(vue)$": "vue-jest",
        "^.+\\.js$": "./node_modules/babel-jest"
    },

    "globals": {
        "_TARGET_": "DEVELOPMENT"
    }

};