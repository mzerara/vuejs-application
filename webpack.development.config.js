// Dependencies
const webpack = require("webpack");
const path = require("path");
const ProgressBarPlugin = require("progress-bar-webpack-plugin");
const chalk = require("chalk");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const WriteFilePlugin = require("write-file-webpack-plugin");
const VueLoaderPlugin = require("vue-loader/lib/plugin");
const VuetifyLoaderPlugin = require("vuetify-loader/lib/plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");


// Directories
const srcFolder = path.resolve(__dirname, "src");
const distFolder = path.resolve(__dirname, "dist");


// Webpack configurations
module.exports = {

    mode: "development",

    devtool: "cheap-module-eval-source-map",

    entry: {
        app: path.resolve(srcFolder, "js/app.js")
    },

    output: {
        path: distFolder,
        filename: "js/[name].bundle.js",
        chunkFilename: "js/[name].chunk.js"
    },

    module: {
        rules: [

            {
                enforce: "pre",
                test: /\.js$/,
                exclude: /node_modules/,
                loader: "eslint-loader"
            },

            {
                test: /\.s(c|a)ss$/,
                use: [{
                        loader: "style-loader"
                    },
                    {
                        loader: "css-loader",
                        options: {
                            sourceMap: true
                        }
                    },
                    {
                        loader: "sass-loader",
                        options: {
                            implementation: require("sass"),
                            sourceMap: true,
                            sassOptions: {
                                fiber: require("fibers"),
                                indentedSyntax: true
                            }
                        }
                    }
                ]
            },

            {
                test: /\.css$/,
                use: [
                    "style-loader",
                    "css-loader"
                ]
            },

            {
                test: /\.vue$/,
                loader: "vue-loader"
            },

            {
                test: /\.(png|jpe?g|bmp|gif)$/,
                loader: "file-loader",
                options: {
                    name: "[name].[ext]",
                    outputPath: "images"
                }
            },

            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: "babel-loader"
            }

        ]
    },

    plugins: [

        new ProgressBarPlugin({
            format: "  build [:bar] " + chalk.green.bold(":percent") + " (:elapsed seconds)",
            clear: false
        }),

        new CleanWebpackPlugin(),


        new HtmlWebpackPlugin({
            template: path.resolve(srcFolder, "index.html"),
            favicon: path.resolve(srcFolder, "favicon.ico")
        }),

        new VuetifyLoaderPlugin(),

        new VueLoaderPlugin(),

        new webpack.DefinePlugin({
            _TARGET_: JSON.stringify("DEVELOPMENT")
        }),

        new WriteFilePlugin()

    ],

    devServer: {
        contentBase: distFolder,
        port: 8080,
        open: true
    }

};