### This is a manually configured project. 

# Instructions

### Setup project

1. Install nodeJS latest version 

2. Clone project  
```code
git clone https://mzerara@bitbucket.org/mzerara/var-frontend.git
```

3. Install project dependencies 
```code
cd var-frontend
npm install
```

### Launch project 

1. Run on development mode
```code
npm run dev 
```