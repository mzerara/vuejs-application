// Dependencies
import axios from 'axios';
import configuration from "./app.configuration";


// HTTP client
const apiClient = axios.create({
    baseURL: configuration.API_URL,
    timeout: 5000
});

export default apiClient;