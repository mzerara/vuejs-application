let configuration = null;

switch (_TARGET_) {

    case "DEVELOPMENT":
        configuration = require("./development.configuration.json");
        break;

    case "PRODUCTION":
        configuration = require("./production.configuration.json");
        break;

    default:
        throw new Error(`Environment has not been set properly. Expecting one of the following values: DEVELOPMENT or PRODUCTION. Received: ${_TARGET_}`);
}

export default configuration;