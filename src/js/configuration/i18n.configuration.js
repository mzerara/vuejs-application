// Set default language
let defaultLanguage = {
    language: "english"
};


switch (defaultLanguage.language) {

    case "english":
        defaultLanguage.content = require("../localization/languages/english.json");
        defaultLanguage.code = "en";
        break;

    case "arabic":
        defaultLanguage.content = require("../localization/languages/arabic.json");
        defaultLanguage.code = "ar";
        break;

    case "french":
        defaultLanguage.content = require("../localization/languages/french.json");
        defaultLanguage.code = "fr";
        break;

    case "berber":
        defaultLanguage.content = require("../localization/languages/berber.json");
        defaultLanguage.code = "br";
        break;

    default:
        throw new Error(`The "${defaultLanguage.language}" default language is not supported!`);

}


// Configurations 
export default {

    default: defaultLanguage,

    supported: [

        {
            code: "en",
            language: "english",
            translation: "ENGLISH",
            htmlSupport: true,
            supported: true
        },

        {
            code: "ar",
            language: "arabic",
            translation: "ARABIC",
            htmlSupport: true,
            supported: true
        },

        {
            code: "fr",
            language: "french",
            translation: "FRENCH",
            htmlSupport: true,
            supported: true
        },

        {
            code: "br",
            language: "berber",
            translation: "TAMAZIGHT",
            htmlSupport: false,
            supported: false
        }

    ]

};