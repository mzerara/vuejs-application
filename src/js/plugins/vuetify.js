// Dependencies
import Vue from 'vue';
import Vuetify from 'vuetify/lib';


// Plugins 
Vue.use(
    Vuetify
);


// Vuetify options 
export default new Vuetify({
    icons: {
        iconfont: 'fa4'
    }
});