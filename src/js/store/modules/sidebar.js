export default {

    namespaced: true,

    state: {
        visible: false,
        header: {
            avatar: "https://randomuser.me/api/portraits/men/78.jpg"
        },
        tabs: [

            {
                name: "home",
                icon: "fa-home",
                route: {
                    name: "home"
                }
            },
            {
                name: "about",
                icon: "fa-info-circle",
                route: {
                    name: "about"
                }
            },
            {
                name: "team",
                icon: "fa-users",
                route: {
                    name: "team"
                }
            },
            {
                name: "contact",
                icon: "fa-phone",
                route: {
                    name: "contact"
                }
            }

        ]
    },

    mutations: {
        TOGGLE_SIDEBAR(state) {
            state.visible = !state.visible;
        }
    },

    actions: {
        //
    },

    getters: {
        //
    }

};