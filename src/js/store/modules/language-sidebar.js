export default {

    namespaced: true,

    state: {
        visible: false,
        width: 48
    },

    mutations: {
        TOGGLE_LANGUAGE_SIDEBAR(state) {
            state.visible = !state.visible;
        }
    },

    actions: {
        //
    },

    getters: {
        //
    }

};