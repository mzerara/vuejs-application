import varLogo from "../../../images/var-logo.png";


export default {

    namespaced: true,

    state: {
        logo: varLogo,
        height: 50
    },

    mutations: {
        //
    },

    actions: {
        //
    },

    getters: {
        //
    }

};