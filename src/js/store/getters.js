export default {

    getSupportedLanguages(state) {
        return state.websiteLanguages.languages.supported.filter((item) => item.supported);
    }

};