export default {

    TOGGLE_LOADER(state, data) {
        if (state.loader.action && state.loader.value) {
            state.loader.action = null;
            state.loader.value = false;
        } else {
            state.loader.action = data.action;
            state.loader.value = true;
        }
    },

    TOGGLE_MODAL(state, data) {
        if (state.modal.window && state.modal.value) {
            state.modal.window = null;
            state.modal.value = false;
        } else {
            state.modal.window = data.window;
            state.modal.value = true;
        }
    },

    SET_APP_FONT_FAMILY(state, data) {
        state.font.type = data.type;
        state.font.family = data.family;
    }

};