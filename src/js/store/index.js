// Dependencies
import Vue from "vue";
import Vuex from "vuex";
import configuration from "../configuration/app.configuration";
import state from "./state";
import mutations from "./mutations";
import actions from "./actions";
import getters from "./getters";
import navbar from "./modules/navbar";
import languageSidebar from "./modules/language-sidebar";
import miniSidebar from "./modules/mini-sidebar";
import sidebar from "./modules/sidebar";


// Plugins 
Vue.use(Vuex);


// Vuex Store
export default new Vuex.Store({
    strict: configuration.VUEX_STRICT_MODE,
    state,
    mutations,
    actions,
    getters,
    modules: {
        navbar,
        languageSidebar,
        miniSidebar,
        sidebar
    }
});