// Dependencies 
import router from "../router";
import loadLanguageAsync from "../localization/i18n.async";
import languages from "../configuration/i18n.configuration";


// Actions 
export default {

    changeWebsiteLanguage({ commit }, data) {
        commit("TOGGLE_LOADER", { action: "refresh-website" });
        router.push({
            params: {
                language: data.code
            }
        }, () => {});
        loadLanguageAsync(data).then(language => {
            if (language === "arabic") {
                commit("SET_APP_FONT_FAMILY", { type: "jooza", family: "flat-jooza, sans-serif" });
            } else {
                commit("SET_APP_FONT_FAMILY", { type: "roboto", family: "roboto, sans-serif" });
            }
            commit("TOGGLE_LOADER");
        });
    },

    setWebsiteLanguage({ dispatch }, data) {
        let websiteLanguage = languages.supported.find((item) => (item.supported) && (item.code === data.route.params.language));
        if (!websiteLanguage) {
            websiteLanguage = languages.supported.find((item) => (item.code === languages.default.code));
        }
        dispatch("changeWebsiteLanguage", websiteLanguage);
    }

};