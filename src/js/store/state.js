// Dependencies 
import languages from "../configuration/i18n.configuration";


// Global state
export default {

    websiteLanguages: {
        languages,
        loadedLanguages: [
            languages.default.code
        ]
    },

    theme: {
        color: "#283e4a",
        contentColor: "#0073b1"
    },

    font: {
        type: "roboto",
        family: "roboto, sans-serif"
    },

    user: {
        loggedIn: false,
        info: null
    },

    loader: {
        action: null,
        value: false
    },

    modal: {
        window: null,
        value: false
    },

    // Delete this once the api is ready!
    categories: [

        {
            label: "education",
            tags: [
                "tag1",
                "tag2",
                "tag3"
            ],
            media: [
                "reading",
                "podcast",
                "video"
            ],
            description: "This is a description for education",
            created_at: "2020-02-21",
            updated_at: "2020-02-21",
            deleted_at: null,
            status: "active"
        },
        {
            label: "technology",
            tags: [
                "tag1",
                "tag2",
                "tag3"
            ],
            media: [
                "reading",
                "podcast",
                "video"
            ],
            description: "This is a description for technology",
            created_at: "2020-02-21",
            updated_at: "2020-02-21",
            deleted_at: null,
            status: "active"
        },
        {
            label: "health",
            tags: [
                "tag1",
                "tag2",
                "tag3"
            ],
            media: [
                "reading",
                "podcast",
                "video"
            ],
            description: "This is a description for health",
            created_at: "2020-02-21",
            updated_at: "2020-02-21",
            deleted_at: null,
            status: "active"
        },
        {
            label: "agriculture",
            tags: [
                "tag1",
                "tag2",
                "tag3"
            ],
            media: [
                "reading",
                "podcast",
                "video"
            ],
            description: "This is a description for agriculture",
            created_at: "2020-02-21",
            updated_at: "2020-02-21",
            deleted_at: null,
            status: "active"
        },
        {
            label: "culture",
            tags: [
                "tag1",
                "tag2",
                "tag3"
            ],
            media: [
                "reading",
                "podcast",
                "video"
            ],
            description: "This is a description for culture",
            created_at: "2020-02-21",
            updated_at: "2020-02-21",
            deleted_at: null,
            status: "active"
        },
        {
            label: "energy",
            tags: [
                "tag1",
                "tag2",
                "tag3"
            ],
            media: [
                "reading",
                "podcast",
                "video"
            ],
            description: "This is a description for energy",
            created_at: "2020-02-21",
            updated_at: "2020-02-21",
            deleted_at: null,
            status: "active"
        },
        {
            label: "environnement",
            tags: [
                "tag1",
                "tag2",
                "tag3"
            ],
            media: [
                "reading",
                "podcast",
                "video"
            ],
            description: "This is a description for environnement",
            created_at: "2020-02-21",
            updated_at: "2020-02-21",
            deleted_at: null,
            status: "active"
        },
        {
            label: "finance",
            tags: [
                "tag1",
                "tag2",
                "tag3"
            ],
            media: [
                "reading",
                "podcast",
                "video"
            ],
            description: "This is a description for finance",
            created_at: "2020-02-21",
            updated_at: "2020-02-21",
            deleted_at: null,
            status: "active"
        },
        {
            label: "industry",
            tags: [
                "tag1",
                "tag2",
                "tag3"
            ],
            media: [
                "reading",
                "podcast",
                "video"
            ],
            description: "This is a description for industry",
            created_at: "2020-02-21",
            updated_at: "2020-02-21",
            deleted_at: null,
            status: "active"
        },
        {
            label: "justice",
            tags: [
                "tag1",
                "tag2",
                "tag3"
            ],
            media: [
                "reading",
                "podcast",
                "video"
            ],
            description: "This is a description for justice",
            created_at: "2020-02-21",
            updated_at: "2020-02-21",
            deleted_at: null,
            status: "active"
        },
        {
            label: "media",
            tags: [
                "tag1",
                "tag2",
                "tag3"
            ],
            media: [
                "reading",
                "podcast",
                "video"
            ],
            description: "This is a description for media",
            created_at: "2020-02-21",
            updated_at: "2020-02-21",
            deleted_at: null,
            status: "active"
        },



        {
            label: "sport",
            tags: [
                "tag1",
                "tag2",
                "tag3"
            ],
            media: [
                "reading",
                "podcast",
                "video"
            ],
            description: "This is a description for sport",
            created_at: "2020-02-21",
            updated_at: "2020-02-21",
            deleted_at: null,
            status: "active"
        },
        {
            label: "tourism",
            tags: [
                "tag1",
                "tag2",
                "tag3"
            ],
            media: [
                "reading",
                "podcast",
                "video"
            ],
            description: "This is a description for tourism",
            created_at: "2020-02-21",
            updated_at: "2020-02-21",
            deleted_at: null,
            status: "active"
        },
        {
            label: "urbanism",
            tags: [
                "tag1",
                "tag2",
                "tag3"
            ],
            media: [
                "reading",
                "podcast",
                "video"
            ],
            description: "This is a description for urbanism",
            created_at: "2020-02-21",
            updated_at: "2020-02-21",
            deleted_at: null,
            status: "active"
        }

    ]

};