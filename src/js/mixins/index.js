// Dependencies 
import router from "../router";
import i18n from "../localization/i18n";


// Common functions 
export default {

    methods: {
        // Navigation
        navigateTo(route) {
            router.push(route, () => {});
        }
    },

    computed: {
        // Conditions
        websiteLanguageIsArabic() {
            return i18n.locale === "arabic" ? true : false;
        },
        justify() {
            return this.websiteLanguageIsArabic ? "text-right" : "text-left";
        }
    }

};