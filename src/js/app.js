import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import i18n from "./localization/i18n";

new Vue({
    router,
    store,
    vuetify,
    i18n,
    name: "App",
    render: h => h(
        App
    )
}).$mount('#app');