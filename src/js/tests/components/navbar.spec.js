// Dependencies
import Navbar from "../../components/Navbar.vue";
import { shallowMount } from "@vue/test-utils";
import {
    localVue,
    createVuexStore,
    createVuetifyInstance,
    createI18nInstance
} from "../test-setup";


// Test suites 
describe("Navbar.vue", () => {

    test("should test all the possible scenarios for 'websiteLanguageIsArabic' computed property", () => {

        // Define possible scenarios
        [

            {
                websiteLanguageIsArabic: true,
                rtlNavigationVisible: true,
                ltrNavigationVisible: false
            },
            {
                websiteLanguageIsArabic: false,
                rtlNavigationVisible: false,
                ltrNavigationVisible: true
            }

        ].forEach(item => {

            // Define wrapper
            const wrapper = shallowMount(Navbar, {
                localVue,
                vuetify: createVuetifyInstance(),
                i18n: createI18nInstance({}),
                mixins: [{
                    computed: {
                        websiteLanguageIsArabic() {
                            return item.websiteLanguageIsArabic;
                        }
                    }
                }],
                store: createVuexStore({
                    state: {
                        theme: {
                            color: "#283e4a"
                        },
                        user: {
                            loggedIn: false
                        }
                    },
                    modules: {
                        navbar: {
                            namespaced: true
                        }
                    }
                })
            });

            // Tests 
            expect(wrapper.find("#rtl__navigation").exists()).toBe(item.rtlNavigationVisible);
            expect(wrapper.find("#ltr__navigation").exists()).toBe(item.ltrNavigationVisible);

        });
    });

    test("should test all the possible scenarios for 'showLeftLanguageSidebarToggle' computed property", () => {

        // Define possible scenarios
        [

            {
                websiteLanguageIsArabic: true,
                userIsloggedIn: true,
                expectedOutput: false,
                rtlToggleVisible: false,
                ltrHamburgerVisible: true,
                rtlLoginVisible: false
            },
            {
                websiteLanguageIsArabic: true,
                userIsloggedIn: false,
                expectedOutput: true,
                rtlToggleVisible: true,
                ltrHamburgerVisible: false,
                rtlLoginVisible: true
            },
            {
                websiteLanguageIsArabic: false,
                userIsloggedIn: true,
                expectedOutput: false,
                rtlToggleVisible: false,
                ltrHamburgerVisible: true,
                rtlLoginVisible: false
            },
            {
                websiteLanguageIsArabic: false,
                userIsloggedIn: false,
                expectedOutput: false,
                rtlToggleVisible: false,
                ltrHamburgerVisible: true,
                rtlLoginVisible: false
            }

        ].forEach(item => {

            // Define wrapper
            const wrapper = shallowMount(Navbar, {
                localVue,
                vuetify: createVuetifyInstance(),
                i18n: createI18nInstance({}),
                mixins: [{
                    computed: {
                        websiteLanguageIsArabic() {
                            return item.websiteLanguageIsArabic;
                        }
                    }
                }],
                store: createVuexStore({
                    state: {
                        theme: {
                            color: "#283e4a"
                        },
                        user: {
                            loggedIn: item.userIsloggedIn
                        }
                    },
                    modules: {
                        navbar: {
                            namespaced: true
                        }
                    }
                })
            });

            // Tests 
            expect(wrapper.vm.showLeftLanguageSidebarToggle).toBe(item.expectedOutput);
            expect(wrapper.find("#rtl__toggle").exists()).toBe(item.rtlToggleVisible);
            expect(wrapper.find("#ltr__hamburger").exists()).toBe(item.ltrHamburgerVisible);
            expect(wrapper.find("#rtl__login").exists()).toBe(item.rtlLoginVisible);

        });

    });

    test("should test all the possible scenarios for 'showRightLanguageSidebarToggle' computed property", () => {

        // Define possible scenarios
        [

            {
                websiteLanguageIsArabic: true,
                userIsloggedIn: true,
                expectedOutput: false,
                ltrToggleVisible: false,
                rtlHamburgerVisible: true

            },
            {
                websiteLanguageIsArabic: true,
                userIsloggedIn: false,
                expectedOutput: false,
                ltrToggleVisible: false,
                rtlHamburgerVisible: true
            },
            {
                websiteLanguageIsArabic: false,
                userIsloggedIn: true,
                expectedOutput: false,
                ltrToggleVisible: false,
                rtlHamburgerVisible: true
            },
            {
                websiteLanguageIsArabic: false,
                userIsloggedIn: false,
                expectedOutput: true,
                ltrToggleVisible: true,
                rtlHamburgerVisible: false
            }

        ].forEach(item => {

            // Define wrapper
            const wrapper = shallowMount(Navbar, {
                localVue,
                vuetify: createVuetifyInstance(),
                i18n: createI18nInstance({}),
                mixins: [{
                    computed: {
                        websiteLanguageIsArabic() {
                            return item.websiteLanguageIsArabic;
                        }
                    }
                }],
                store: createVuexStore({
                    state: {
                        theme: {
                            color: "#283e4a"
                        },
                        user: {
                            loggedIn: item.userIsloggedIn
                        }
                    },
                    modules: {
                        navbar: {
                            namespaced: true
                        }
                    }
                })
            });

            // Tests 
            expect(wrapper.vm.showRightLanguageSidebarToggle).toBe(item.expectedOutput);
            expect(wrapper.find("#ltr__toggle").exists()).toBe(item.ltrToggleVisible);
            expect(wrapper.find("#rtl__hamburger").exists()).toBe(item.rtlHamburgerVisible);

        });

    });

});