// Dependencies
import MiniSidebar from "../../components/MiniSidebar.vue";
import { shallowMount } from '@vue/test-utils';
import { localVue, createVuexStore, createI18nInstance, createVuetifyInstance } from "../test-setup";


// Test suites 
describe('MiniSidebar.vue', () => {

    let wrapper;

    beforeEach(() => {

        wrapper = shallowMount(MiniSidebar, {
            localVue,
            vuetify: createVuetifyInstance(),
            i18n: createI18nInstance({}),
            store: createVuexStore({
                //
            })
        });

    });

    test('should ', () => {
        console.log(wrapper);
    });

});