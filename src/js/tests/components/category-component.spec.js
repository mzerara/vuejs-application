// Dependencies
import CategoryButton from "../../components/buttons/CategoryButton.vue";
import { mount } from "@vue/test-utils";
import {
    localVue,
    createI18nInstance
} from "../test-setup";


// Test suites 
describe("CategoryButton.vue", () => {

    test("should uppercase 'label' in category prop to 'HEALTH'", () => {

        // Define wrapper
        const wrapper = mount(CategoryButton, {
            localVue,
            i18n: createI18nInstance({}),
            propsData: {
                category: {
                    label: "health"
                }
            }
        });

        // Tests 
        expect(wrapper.vm.upperCasedCategory).toEqual("HEALTH");

    });

});