// Dependencies
import LocalizationButton from "../../components/buttons/LocalizationButton.vue";
import { mount } from "@vue/test-utils";
import { localVue } from "../test-setup";


// Test suites 
describe("LocalizationButton.vue", () => {

    test("should call 'TOGGLE_LANGUAGE_SIDEBAR' mutation", () => {

        // Define wrapper and set locale to english 
        const wrapper = mount(LocalizationButton, {
            localVue
        });

        // Tests 
        expect(wrapper.find("button").exists()).toBe(true);
        const vBtn = wrapper.find("button");
        spyOn(wrapper.vm, "TOGGLE_LANGUAGE_SIDEBAR");
        vBtn.trigger("click");
        expect(wrapper.vm.TOGGLE_LANGUAGE_SIDEBAR).toHaveBeenCalled();

    });

});