// Dependencies
import SidebarHeader from "../../components/SidebarHeader.vue";
import { mount } from "@vue/test-utils";
import {
    localVue,
    createVuexStore
} from "../test-setup";


// Test suites 
describe("SidebarHeader.vue", () => {

    test("should test 'sidebarHeaderStyles' computed property", () => {

        // Define wrapper
        const wrapper = mount(SidebarHeader, {
            localVue,
            store: createVuexStore({
                modules: {
                    navbar: {
                        namespaced: true,
                        state: {
                            height: 65
                        }
                    },
                    sidebar: {
                        namespaced: true,
                        state: {
                            header: {
                                avatar: "test-avatar.jpg"
                            }
                        }
                    }
                }
            })
        });

        // Tests 
        expect(wrapper.vm.sidebarHeaderStyles).toEqual({
            height: `${wrapper.vm.$store.state.navbar.height}px`
        });

    });

    test("should test all the possible scenarios for 'avatarOrder' computed property", () => {

        // Define possible scenarios
        [

            {
                websiteLanguageIsArabic: true,
                expectedOutput: 2

            },
            {
                websiteLanguageIsArabic: false,
                expectedOutput: 1

            }


        ].forEach(item => {

            // Define wrapper
            const wrapper = mount(SidebarHeader, {
                localVue,
                mixins: [{
                    computed: {
                        websiteLanguageIsArabic() {
                            return item.websiteLanguageIsArabic;
                        }
                    }
                }],
                store: createVuexStore({
                    modules: {
                        navbar: {
                            namespaced: true,
                            state: {
                                height: 65
                            }
                        },
                        sidebar: {
                            namespaced: true,
                            state: {
                                header: {
                                    avatar: "test-avatar.jpg"
                                }
                            }
                        }
                    }
                })
            });

            // Tests 
            expect(wrapper.vm.avatarOrder).toBe(item.expectedOutput);

        });

    });

    test("should test all the possible scenarios for 'usernameOrder' computed property", () => {

        // Define possible scenarios
        [

            {
                websiteLanguageIsArabic: true,
                expectedOutput: 1

            },
            {
                websiteLanguageIsArabic: false,
                expectedOutput: 2

            }


        ].forEach(item => {

            // Define wrapper
            const wrapper = mount(SidebarHeader, {
                localVue,
                mixins: [{
                    computed: {
                        websiteLanguageIsArabic() {
                            return item.websiteLanguageIsArabic;
                        }
                    }
                }],
                store: createVuexStore({
                    modules: {
                        navbar: {
                            namespaced: true,
                            state: {
                                height: 65
                            }
                        },
                        sidebar: {
                            namespaced: true,
                            state: {
                                header: {
                                    avatar: "test-avatar.jpg"
                                }
                            }
                        }
                    }
                })
            });

            // Tests 
            expect(wrapper.vm.usernameOrder).toBe(item.expectedOutput);

        });

    });

});