// Dependencies
import NavigationButton from "../../components/buttons/NavigationButton.vue";
import { mount } from "@vue/test-utils";
import {
    localVue,
    createI18nInstance
} from "../test-setup";


// Test suites 
describe("NavigationButton.vue", () => {

    test("should uppercase 'about' route prop to 'ABOUT' ", () => {

        // Define wrapper
        const wrapper = mount(NavigationButton, {
            localVue,
            i18n: createI18nInstance({}),
            propsData: {
                route: "about"
            }
        });

        // Tests 
        expect(wrapper.vm.upperCasedRoute).toEqual("ABOUT");

    });

    test("should navigateTo 'about' page for right to left written languages", () => {

        // Define wrapper
        const wrapper = mount(NavigationButton, {
            localVue,
            i18n: createI18nInstance({
                locale: "arabic",
                fallbackLocale: "arabic",
                messages: {
                    language: "arabic",
                    content: {
                        NAVBAR_COMPONENT: {
                            TABS: {
                                ABOUT: "من نحن"
                            }
                        }
                    }
                }
            }),
            mixins: [{
                methods: {
                    navigateTo: route => {
                        return route;
                    }
                }
            }],
            propsData: {
                route: "about"
            }
        });

        // Tests 
        expect(wrapper.vm.$i18n.locale).toEqual("arabic");
        expect(wrapper.find("button").exists()).toBe(true);
        const vBtn = wrapper.find("button");
        expect(vBtn.text()).toEqual("م ن نحن");
        spyOn(wrapper.vm, "navigateTo");
        vBtn.trigger("click");
        expect(wrapper.vm.navigateTo).toHaveBeenCalledWith({ name: "about" });

    });

    test("should navigateTo about page for left to right written languages", () => {

        // Define wrapper
        const wrapper = mount(NavigationButton, {
            localVue,
            i18n: createI18nInstance({
                locale: "english",
                fallbackLocale: "english",
                messages: {
                    language: "english",
                    content: {
                        NAVBAR_COMPONENT: {
                            TABS: {
                                ABOUT: "about"
                            }
                        }
                    }
                }
            }),
            mixins: [{
                methods: {
                    navigateTo: route => {
                        return route;
                    }
                }
            }],
            propsData: {
                route: "about"
            }
        });

        // Tests 
        expect(wrapper.vm.$i18n.locale).toEqual("english");
        expect(wrapper.find("button").exists()).toBe(true);
        const vBtn = wrapper.find("button");
        expect(vBtn.text()).toEqual("a bout");
        spyOn(wrapper.vm, "navigateTo");
        vBtn.trigger("click");
        expect(wrapper.vm.navigateTo).toHaveBeenCalledWith({ name: "about" });

    });

});