// Dependencies
import Sidebar from "../../components/Sidebar.vue";
import { shallowMount } from "@vue/test-utils";
import {
    localVue,
    createVuexStore,
} from "../test-setup";


// Test suites 
describe('Sidebar.vue', () => {

    test("should test that 'hideSidebar' method calls 'TOGGLE_SIDEBAR' mutation", () => {

        // Define wrapper
        const wrapper = shallowMount(Sidebar, {
            localVue,
            store: createVuexStore({
                state: {
                    theme: {
                        color: "#283e4a"
                    }
                },
                modules: {
                    sidebar: {
                        namespaced: true
                    }
                }
            })
        });

        // Tests 
        spyOn(wrapper.vm, "TOGGLE_SIDEBAR");
        wrapper.vm.hideSidebar(false);
        expect(wrapper.vm.TOGGLE_SIDEBAR).toHaveBeenCalled();

    });

});