// Dependencies
import LanguageSidebar from "../../components/LanguageSidebar.vue";
import { shallowMount } from "@vue/test-utils";
import {
    localVue,
    createVuexStore,
    createVuetifyInstance,
    createI18nInstance
} from "../test-setup";


// Test suites 
describe("LanguageSidebar.vue", () => {

    test("should test that 'hideLanguageSidebar' method calls 'TOGGLE_LANGUAGE_SIDEBAR' mutation", () => {

        // Define wrapper
        const wrapper = shallowMount(LanguageSidebar, {
            localVue,
            vuetify: createVuetifyInstance(),
            i18n: createI18nInstance({}),
            store: createVuexStore({
                state: {
                    theme: {
                        color: "#283e4a"
                    }
                },
                getters: {
                    getSupportedLanguages: () => []
                },
                modules: {
                    navbar: {
                        namespaced: true
                    },
                    languageSidebar: {
                        namespaced: true
                    }
                }
            })
        });

        // Tests 
        spyOn(wrapper.vm, "TOGGLE_LANGUAGE_SIDEBAR");
        wrapper.vm.hideLanguageSidebar(false);
        expect(wrapper.vm.TOGGLE_LANGUAGE_SIDEBAR).toHaveBeenCalled();

    });

    test("should test that 'hideLanguageSidebar' method doesn't call 'TOGGLE_LANGUAGE_SIDEBAR' mutation", () => {

        // Define wrapper
        const wrapper = shallowMount(LanguageSidebar, {
            localVue,
            vuetify: createVuetifyInstance(),
            i18n: createI18nInstance({}),
            store: createVuexStore({
                state: {
                    theme: {
                        color: "#283e4a"
                    }
                },
                getters: {
                    getSupportedLanguages: () => []
                },
                modules: {
                    navbar: {
                        namespaced: true
                    },
                    languageSidebar: {
                        namespaced: true
                    }
                }
            })
        });

        // Tests 
        spyOn(wrapper.vm, "TOGGLE_LANGUAGE_SIDEBAR");
        wrapper.vm.hideLanguageSidebar(true);
        expect(wrapper.vm.TOGGLE_LANGUAGE_SIDEBAR).not.toHaveBeenCalled();

    });

    test("should test all the possible scenarios for 'supportedLanguages' computed property", () => {

        // Define possible scenarios
        [

            {
                websiteLanguage: {
                    locale: "english",
                    fallbackLocale: "english",
                    messages: {
                        language: "english",
                        content: require("../../localization/languages/english.json")
                    }
                },
                expectedOutput: [

                    {
                        language: "arabic",
                        translation: "ARABIC"
                    },
                    {
                        language: "french",
                        translation: "FRENCH"
                    }

                ]
            },
            {
                websiteLanguage: {
                    locale: "arabic",
                    fallbackLocale: "arabic",
                    messages: {
                        language: "arabic",
                        content: require("../../localization/languages/arabic.json")
                    }
                },

                expectedOutput: [

                    {
                        language: "english",
                        translation: "ENGLISH"
                    },
                    {
                        language: "french",
                        translation: "FRENCH"
                    }

                ]
            }

        ].forEach(item => {

            // Define wrapper
            const wrapper = shallowMount(LanguageSidebar, {
                localVue,
                vuetify: createVuetifyInstance(),
                i18n: createI18nInstance(item.websiteLanguage),
                store: createVuexStore({
                    state: {
                        theme: {
                            color: "#283e4a"
                        }
                    },
                    getters: {
                        getSupportedLanguages() {
                            return [

                                {
                                    language: "english",
                                    translation: "ENGLISH"
                                },
                                {
                                    language: "arabic",
                                    translation: "ARABIC"
                                },
                                {
                                    language: "french",
                                    translation: "FRENCH"
                                }

                            ];
                        }
                    },
                    modules: {
                        navbar: {
                            namespaced: true
                        },
                        languageSidebar: {
                            namespaced: true
                        }
                    }
                })
            });

            // Tests 
            expect(wrapper.vm.supportedLanguages).toEqual(item.expectedOutput);

        });

    });

    test("should test all the possible scenarios for 'sidebarStyles' computed property", () => {

        // Define possible scenarios
        [

            {
                websiteLanguageIsArabic: true,
                expectedOutput: {
                    marginTop: "65px",
                    height: "0px",
                    borderBottomRightRadius: "7px",
                    borderBottomLeftRadius: "0"
                }
            },
            {
                websiteLanguageIsArabic: false,
                expectedOutput: {
                    marginTop: "65px",
                    height: "0px",
                    borderBottomRightRadius: "0",
                    borderBottomLeftRadius: "7px"
                }
            }

        ].forEach(item => {

            // Define wrapper
            const wrapper = shallowMount(LanguageSidebar, {
                localVue,
                vuetify: createVuetifyInstance(),
                i18n: createI18nInstance({}),
                mixins: [{
                    computed: {
                        websiteLanguageIsArabic() {
                            return item.websiteLanguageIsArabic;
                        }
                    }
                }],
                store: createVuexStore({
                    state: {
                        theme: {
                            color: "#283e4a"
                        }
                    },
                    getters: {
                        getSupportedLanguages() {
                            return [

                                {
                                    code: "en",
                                    language: "english",
                                    translation: "ENGLISH",
                                    htmlSupport: true,
                                    supported: true
                                }

                            ];
                        }
                    },
                    modules: {
                        navbar: {
                            namespaced: true,
                            state: {
                                height: 65
                            }
                        },
                        languageSidebar: {
                            namespaced: true,
                            state: {
                                width: 48
                            }
                        }
                    }
                })
            });

            // Tests 
            expect(wrapper.vm.sidebarStyles).toEqual(item.expectedOutput);

        });

    });

});