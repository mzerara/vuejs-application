// Dependencies
import SidebarButton from "../../components/buttons/SidebarButton.vue";
import { mount } from "@vue/test-utils";
import {
    localVue,
    createI18nInstance
} from "../test-setup";


// Test suites 
describe("SidebarButton.vue", () => {

    test("should navigateTo 'about' page", () => {

        // Define wrapper
        const wrapper = mount(SidebarButton, {
            localVue,
            i18n: createI18nInstance({}),
            mixins: [{
                methods: {
                    navigateTo: route => {
                        return route;
                    }
                }
            }],
            propsData: {
                tab: {
                    name: "about",
                    icon: "fa-info-circle",
                    route: {
                        name: "about"
                    }
                }
            }
        });

        // Tests 
        expect(wrapper.find(".v-list-item").exists()).toBe(true);
        const vList = wrapper.find(".v-list-item");
        spyOn(wrapper.vm, "navigateTo");
        vList.trigger("click");
        expect(wrapper.vm.navigateTo).toHaveBeenCalledWith({ name: "about" });

    });

    test("should uppercase 'about.name' tab prop to 'ABOUT'", () => {

        // Define wrapper
        const wrapper = mount(SidebarButton, {
            localVue,
            i18n: createI18nInstance({}),
            propsData: {
                tab: {
                    name: "about",
                    icon: "fa-info-circle",
                    route: {
                        name: "about"
                    }
                }
            }
        });

        // Tests 
        expect(wrapper.vm.upperCasedTab).toEqual("ABOUT");

    });

    test("should test all the possible scenarios for 'iconOrder' computed property", () => {

        // Define possible scenarios
        [

            {
                websiteLanguageIsArabic: true,
                expectedOutput: 2

            },
            {
                websiteLanguageIsArabic: false,
                expectedOutput: 1

            }


        ].forEach(item => {

            // Define wrapper
            const wrapper = mount(SidebarButton, {
                localVue,
                i18n: createI18nInstance({}),
                mixins: [{
                    computed: {
                        websiteLanguageIsArabic() {
                            return item.websiteLanguageIsArabic;
                        }
                    }
                }],
                propsData: {
                    tab: {
                        name: "about",
                        icon: "fa-info-circle",
                        route: {
                            name: "about"
                        }
                    }
                }
            });

            // Tests 
            expect(wrapper.vm.iconOrder).toBe(item.expectedOutput);

        });

    });

    test("should test all the possible scenarios for 'nameOrder' computed property", () => {

        // Define possible scenarios
        [

            {
                websiteLanguageIsArabic: true,
                expectedOutput: 1

            },
            {
                websiteLanguageIsArabic: false,
                expectedOutput: 2

            }


        ].forEach(item => {

            // Define wrapper
            const wrapper = mount(SidebarButton, {
                localVue,
                i18n: createI18nInstance({}),
                mixins: [{
                    computed: {
                        websiteLanguageIsArabic() {
                            return item.websiteLanguageIsArabic;
                        }
                    }
                }],
                propsData: {
                    tab: {
                        name: "about",
                        icon: "fa-info-circle",
                        route: {
                            name: "about"
                        }
                    }
                }
            });

            // Tests 
            expect(wrapper.vm.nameOrder).toBe(item.expectedOutput);

        });

    });

});