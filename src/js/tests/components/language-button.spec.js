// Dependencies
import LanguageButton from "../../components/buttons/LanguageButton.vue";
import { mount } from "@vue/test-utils";
import {
    localVue,
    createI18nInstance
} from "../test-setup";


// Test suites 
describe("LanguageButton.vue", () => {

    test("should test that 'changeWebsiteLanguage' method is called on a click event", () => {

        // Define wrapper
        const wrapper = mount(LanguageButton, {
            localVue,
            i18n: createI18nInstance({}),
            propsData: {
                language: {
                    code: "en",
                    translation: "ENGLISH"
                }
            }
        });

        // Tests 
        const vList = wrapper.find(".v-list-item");
        spyOn(wrapper.vm, "changeWebsiteLanguage");
        vList.trigger("click");
        expect(wrapper.vm.changeWebsiteLanguage).toHaveBeenCalledWith({
            code: "en",
            translation: "ENGLISH"
        });

    });

});