// Dependencies
import HamburgerButton from "../../components/buttons/HamburgerButton.vue";
import { mount } from "@vue/test-utils";
import { localVue } from "../test-setup";


// Test suites 
describe("HamburgerButton.vue", () => {

    test("should call 'TOGGLE_SIDEBAR' mutation", () => {

        // Define wrapper and set locale to english 
        const wrapper = mount(HamburgerButton, {
            localVue
        });

        // Tests
        expect(wrapper.find(".v-btn").exists()).toBe(true);
        const vBtn = wrapper.find(".v-btn");
        spyOn(wrapper.vm, "TOGGLE_SIDEBAR");
        vBtn.trigger("click");
        expect(wrapper.vm.TOGGLE_SIDEBAR).toHaveBeenCalled();

    });

});