// Dependencies
import Vue from "vue";
import Vuex from 'vuex';
import Vuetify from 'vuetify';
import VueI18n from "vue-i18n";
import languages from "../configuration/i18n.configuration";
import { createLocalVue } from '@vue/test-utils';


// Local Vue instance 
const localVue = createLocalVue();


// Plugins 
Vue.use(Vuetify);
localVue.use(Vuex);
localVue.use(VueI18n);
localVue.use(Vuetify);


// Create a running store 
export let createVuexStore = (data) => {

    return new Vuex.Store({
        strict: false,
        state: data.state ? data.state : {},
        mutations: data.mutations ? data.mutations : {},
        actions: data.actions ? data.actions : {},
        getters: data.getters ? data.getters : {},
        modules: data.modules ? data.modules : {}
    });

};


// Create an I18n instance 
export let createI18nInstance = (data) => {

    let messages;

    if (data.messages) {
        messages = {
            [data.messages.language]: data.messages.content
        };
    } else {
        messages = {
            [languages.default.language]: languages.default.content
        };
    }

    return new VueI18n({
        locale: data.locale ? data.locale : languages.default.language,
        fallbackLocale: data.fallbackLocale ? data.fallbackLocale : languages.default.language,
        messages
    });

};


// Create an I18n instance 
export let createVuetifyInstance = () => {

    return new Vuetify({
        icons: {
            iconfont: 'fa4'
        }
    });

};


export { localVue };