// Dependencies
import miniSidebar from "../../store/modules/mini-sidebar";
import { createVuexStore } from "../test-setup";


// Test suites 
describe("mutations", () => {

    test("should test mutations", () => {

        // Create a running store 
        //const store = 
        createVuexStore({
            state: {
                //
            },
            mutations: miniSidebar.mutations,
            actions: miniSidebar.actions,
            getters: miniSidebar.getters
        });
    });

});

describe("actions", () => {

    test("should test actions", () => {

        // Create a running store 
        //const store = 
        createVuexStore({
            state: {
                //
            },
            mutations: miniSidebar.mutations,
            actions: miniSidebar.actions,
            getters: miniSidebar.getters
        });
    });

});

describe("getters", () => {

    test("should test getters", () => {

        // Create a running store 
        //const store = 
        createVuexStore({
            state: {
                //
            },
            mutations: miniSidebar.mutations,
            actions: miniSidebar.actions,
            getters: miniSidebar.getters
        });
    });

});