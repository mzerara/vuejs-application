// Dependencies
import navbar from "../../store/modules/navbar";
import { createVuexStore } from "../test-setup";


// Test suites 
describe("mutations", () => {

    test("should test mutations", () => {

        // Create a running store 
        // const store = 
        createVuexStore({
            state: {
                //
            },
            mutations: navbar.mutations,
            actions: navbar.actions,
            getters: navbar.getters
        });
    });

});

describe("actions", () => {

    test("should test actions", () => {

        // Create a running store 
        // const store = 
        createVuexStore({
            state: {
                //
            },
            mutations: navbar.mutations,
            actions: navbar.actions,
            getters: navbar.getters
        });
    });

});

describe("getters", () => {

    test("should test getters", () => {

        // Create a running store 
        // const store = 
        createVuexStore({
            state: {
                //
            },
            mutations: navbar.mutations,
            actions: navbar.actions,
            getters: navbar.getters
        });
    });

});