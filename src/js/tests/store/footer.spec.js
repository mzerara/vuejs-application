// Dependencies
import footer from "../../store/modules/footer";
import { createVuexStore } from "../test-setup";


// Test suites 
describe("mutations", () => {

    test("should test mutations", () => {

        // Create a running store 
        //const store = 
        createVuexStore({
            state: {
                //
            },
            mutations: footer.mutations,
            actions: footer.actions,
            getters: footer.getters
        });
    });

});

describe("actions", () => {

    test("should test actions", () => {

        // Create a running store 
        //const store = 
        createVuexStore({
            state: {
                //
            },
            mutations: footer.mutations,
            actions: footer.actions,
            getters: footer.getters
        });
    });

});

describe("getters", () => {

    test("should test getters", () => {

        // Create a running store 
        //const store = 
        createVuexStore({
            state: {
                //
            },
            mutations: footer.mutations,
            actions: footer.actions,
            getters: footer.getters
        });
    });

});