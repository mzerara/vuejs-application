// Dependencies
import languageSidebar from "../../store/modules/language-sidebar";
import { createVuexStore } from "../test-setup";


// Test suites  
describe("mutations", () => {

    test("should show language sidebar", () => {

        // Create a running store 
        const store = createVuexStore({
            state: {
                visible: false
            },
            mutations: languageSidebar.mutations
        });

        //tests 
        store.commit("TOGGLE_LANGUAGE_SIDEBAR");
        expect(store.state.visible).toBe(true);

    });

    test("should hide language sidebar", () => {

        // Create a running store 
        const store = createVuexStore({
            state: {
                visible: true
            },
            mutations: languageSidebar.mutations
        });

        //tests 
        store.commit("TOGGLE_LANGUAGE_SIDEBAR");
        expect(store.state.visible).toBe(false);

    });

});