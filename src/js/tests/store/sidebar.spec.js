// Dependencies
import sidebar from "../../store/modules/sidebar";
import { createVuexStore } from "../test-setup";


// Test suites  
describe("mutations", () => {

    test("should show sidebar", () => {

        // Create a running store 
        const store = createVuexStore({
            state: {
                visible: false
            },
            mutations: sidebar.mutations
        });

        //tests 
        store.commit("TOGGLE_SIDEBAR");
        expect(store.state.visible).toBe(true);

    });

    test("should hide sidebar", () => {

        // Create a running store 
        const store = createVuexStore({
            state: {
                visible: true
            },
            mutations: sidebar.mutations
        });

        //tests 
        store.commit("TOGGLE_SIDEBAR");
        expect(store.state.visible).toBe(false);

    });

});