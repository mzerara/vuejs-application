// Dependencies
import mutations from "../../store/mutations";
import actions from "../../store/actions";
import getters from "../../store/getters";
import { createVuexStore } from "../test-setup";


// Test suites 
describe("mutations", () => {

    test("should run loader", () => {

        // Create a running store 
        const store = createVuexStore({
            state: {
                loader: {
                    action: null,
                    value: false
                }
            },
            mutations
        });

        // Tests 
        store.commit("TOGGLE_LOADER", {
            action: "mock"
        });
        expect(store.state.loader).toEqual({
            action: "mock",
            value: true
        });

    });

    test("should stop loader", () => {

        // Create a running store 
        const store = createVuexStore({
            state: {
                loader: {
                    action: "mock",
                    value: true
                }
            },
            mutations
        });

        // Tests 
        store.commit("TOGGLE_LOADER");
        expect(store.state.loader).toEqual({
            action: null,
            value: false
        });

    });

    test("should show modal", () => {

        // Create a running store 
        const store = createVuexStore({
            state: {
                modal: {
                    window: null,
                    value: false
                }
            },
            mutations
        });

        // Tests 
        store.commit("TOGGLE_MODAL", {
            window: "mock-modal"
        });
        expect(store.state.modal).toEqual({
            window: "mock-modal",
            value: true
        });

    });

    test("should hide modal", () => {

        // Create a running store 
        const store = createVuexStore({
            state: {
                modal: {
                    window: "mock-modal",
                    value: true
                }
            },
            mutations
        });

        // Tests 
        store.commit("TOGGLE_MODAL");
        expect(store.state.modal).toEqual({
            window: null,
            value: false
        });

    });

    test("should set app font family", () => {

        // Create a running store 
        const store = createVuexStore({
            state: {
                font: {
                    type: "roboto",
                    family: "roboto, sans-serif"
                }
            },
            mutations
        });

        // Tests 
        store.commit("SET_APP_FONT_FAMILY", {
            type: "mock-type",
            family: "mock-family"
        });
        expect(store.state.font).toEqual({
            type: "mock-type",
            family: "mock-family"
        });

    });

});


describe("actions", () => {

    test("should test actions", () => {

        // Create a running store 
        // const store = 
        createVuexStore({
            state: {
                //
            },
            actions
        });

    });

});


describe("getters", () => {

    test("should get supported languages", () => {

        // Create a running store 
        const store = createVuexStore({
            state: {
                websiteLanguages: {
                    languages: {
                        supported: [

                            {
                                language: "english",
                                supported: true
                            },
                            {
                                language: "arabic",
                                supported: true
                            },
                            {
                                language: "berber",
                                supported: false
                            }

                        ]
                    }
                }
            },
            getters
        });

        // Tests
        expect(store.getters.getSupportedLanguages).toEqual([

            {
                language: "english",
                supported: true
            },
            {
                language: "arabic",
                supported: true
            }

        ]);

    });

});