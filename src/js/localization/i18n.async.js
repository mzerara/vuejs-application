// Dependencies 
import i18n from "./i18n";
import apiClient from "../configuration/http-client.configuration";
import languages from "../configuration/i18n.configuration";


// Set default language 
const loadedLanguages = [languages.default.code];


// Enable code splitting
let setI18nLanguage = data => {
    i18n.locale = data.language;
    if (data.htmlSupport) {
        apiClient.defaults.headers.common['Accept-Language'] = data.code;
        document.querySelector('html').setAttribute('lang', data.code);
    } else {
        apiClient.defaults.headers.common['Accept-Language'] = "";
        document.querySelector('html').setAttribute('lang', "");
    }
    return data.language;
};


export default data => {
    if (i18n.locale !== data.language) {
        if (!loadedLanguages.includes(data.language)) {
            return import ( /* webpackChunkName: "language-[request]" */ `./languages/${data.language}.json`).then(
                content => {
                    i18n.setLocaleMessage(data.language, content.default);
                    loadedLanguages.push(data.language);
                    return setI18nLanguage(data);
                }
            );
        }
        return Promise.resolve(setI18nLanguage(data));
    }
    return Promise.resolve(setI18nLanguage(data));
};