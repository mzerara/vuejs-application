// Dependencies
import Vue from "vue";
import VueI18n from "vue-i18n";
import languages from "../configuration/i18n.configuration";


// Plugins 
Vue.use(VueI18n);


//i18n-vue setup 
export default new VueI18n({
    locale: languages.default.language,
    fallbackLocale: languages.default.language,
    messages: {
        [languages.default.language]: languages.default.content
    }
});