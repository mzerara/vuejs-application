// Dependencies
import Vue from "vue";
import VueRouter from "vue-router";
import routes from "./routes";
import configuration from "../configuration/app.configuration";


// Plugins 
Vue.use(VueRouter);


// Vue-router
export default new VueRouter({
    mode: configuration.VUE_ROUTER_MODE,
    routes
});