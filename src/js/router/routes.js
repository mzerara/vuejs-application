// Dependencies
import languages from "../configuration/i18n.configuration";


// Routes 
export default [

    {
        path: "/",
        redirect: {
            name: "home",
            params: {
                language: languages.default.code
            }
        }
    },

    {
        path: "/:language",
        name: "root",
        redirect: {
            name: "home"
        },
        component: {
            render: h => h('router-view')
        },
        children: [

            {
                path: 'home',
                name: 'home',
                component: () =>
                    import (
                        /* webpackChunkName: 'home' */
                        '../views/Home.vue'
                    )
            },

            {
                path: 'about',
                name: 'about',
                component: () =>
                    import (
                        /* webpackChunkName: 'about' */
                        '../views/About.vue'
                    )
            },

            {
                path: 'team',
                name: 'team',
                component: () =>
                    import (
                        /* webpackChunkName: 'team' */
                        '../views/Team.vue'
                    )
            },

            {
                path: 'contact',
                name: 'contact',
                component: () =>
                    import (
                        /* webpackChunkName: 'contact' */
                        '../views/Contact.vue'
                    )
            },

            {
                path: 'login',
                name: 'login',
                component: () =>
                    import (
                        /* webpackChunkName: 'login' */
                        '../views/Login.vue'
                    )
            },

            {
                path: 'dashboard',
                name: 'dashboard',
                component: () =>
                    import (
                        /* webpackChunkName: 'dashboard' */
                        '../views/Dashboard.vue'
                    )
            },
            {
                path: 'profile',
                name: 'profile',
                component: () =>
                    import (
                        /* webpackChunkName: 'profile' */
                        '../views/Profile.vue'
                    )
            },

            {
                path: "*",
                redirect: {
                    name: "home"
                }
            }

        ]
    }

];